from TTT import TicTacToe

def main():

    playAgain = "y"

    while playAgain == 'y' :
        n = int(input("What NxN Tic Tac Toe game do you want: "))
        game = TicTacToe(n)
        game.play()

        #run the main driver here
        playAgain = input("Do you want to play again? [y/n] :: ")

if __name__ == "__main__" :
    main()