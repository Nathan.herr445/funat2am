# Copyright (C) 2020
# 
# This file is part of UTCS Free Software Foundation.
# 
#  is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# 
#  is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with UTCS Free Software Foundation. If not, see 
# <http://www.gnu.org/licenses/>.

from enum import Enum
from random import choice
from typing import Iterator, List, Match, Optional, Tuple
from itertools import cycle, dropwhile
from operator import is_, eq
from functools import partial

import re
import random
import time

r"""
 _  _  ___    ___ ___   ___ _____ _ _____ ___ __  __ ___ _  _ _____ ___ 
| \| |/ _ \  |_ _| __| / __|_   _/_\_   _| __|  \/  | __| \| |_   _/ __|
| .` | (_) |  | || _|  \__ \ | |/ _ \| | | _|| |\/| | _|| .` | | | \__ \
|_|\_|\___/  |___|_|   |___/ |_/_/ \_\_| |___|_|  |_|___|_|\_| |_| |___/
unless its a precondition
also ternaries
or if they are just there
"""

def check_none(value: object, name: str):
    """
    Checks if a value is not none and raises a ValueError if it is null

    Parameters:
        value (object): The value to check 
        name (str): The name
    """
    if object is None:
        raise ValueError(f"{name} cannot be None")


class Player(Enum):
    NONE = "-"
    X = "X"
    O = "O"
    
    def __str__(self):
        return self.value

Spot = Tuple[int, int]

class InputParser:
    """
    A class to collect information about what placement the user wants to make

    Methods
    -------
    parse_input():
        Parses the user's response to the prompt
    """
    input_syntax = re.compile(r"^\s*(?P<row>[0-9]+),?\s*(?P<col>[0-9]+)\s*$")
    
    def __init__(self, prompt: str, error_message: str = 'Invalid input!'):
        '''
        Creates a new InputParser which parses the response to the given prompt.
        
        Parameters:
            prompt (str): The prompt to give the user
            error_message (str): The error message to be displayed if the user provides invalid input
            (default is 'Invalid input!')
        '''
        self.__prompt = prompt
        self.__error_message = error_message

    def parse_input(self) -> Optional[Spot]:
        """
        Parses the users input and returns a location for where the position is
        """
        match = InputParser.input_syntax.match(input(self.__prompt))        
        return ((int(match.group('row')), int(match.group('col'))) 
                if match is not None 
                else self.__print_error())

    def __print_error(self):
        print(self.__error_message)

class Board:
    """
    A class to represent a Tic Tac Toe board.

    Methods
    -------
    place(player, spot):
        Places an X or O on the board.

    winner():
        Determines the winner of this Tic Tac Toe board.        
    """
    
    def __init__(self, n: int):
        '''
        Creates an n x n Tic Tac Toe board.
        
        Parameters:
            n (int): The width and height of the board
        '''
        if n <= 0:
            raise ValueError('Invalid value for "n"')
        
        self.__n = n
        self.__empty_spots = (n ** 2)
        self.__board = [Player.NONE] * self.__empty_spots
        
    def __iter__(self):
        return iter(self.__board)
        
    def place(self, player: Player, spot: Spot):
        '''
        Places an X or O on the board.
        
        Parameters:
            player (Player): Either an X or O
            spot (Spot): The spot at which the placement will be made
        '''
        check_none(player, "Player")
        check_none(spot, "Spot")
        
        row, col = (s - 1 for s in spot) # same as spot[0] - 1, spot[1] - 1 
        if not (0 <= row < self.__n and 0 <= col < self.__n):
            raise ValueError('Spot out of bounds')
        
        if spot in self.available_spots():
            self.__board[row * self.__n + col] = player
            self.__empty_spots -= 1

    # needs to return X, O, draw, or no winner yet
    # should cache between turns
    def winner(self) -> Optional[Player]:
        '''
        Determines the winner of this Tic Tac Toe board.

        Returns
            winner (Player): Either Player.X, Player.O, Player.NONE for draw, or None if there is
            no winner yet
        '''       
        slices = [
            # Horizontal
            *[self.__board[x:x+self.__n] for x in range(self.__n)],
            # Vertical
            *[self.__board[x::self.__n] for x in range(self.__n)],
            # Forward Diagonal
            self.__board[::self.__n + 1],
            # Backwards Diagonal
            self.__board[self.__n - 1:(self.__n ** 2) - 1:self.__n - 1],
        ]

        # FCC THanks giving!!!
        winner = next((slice[0] for slice in slices 
                        if slice[0] is not Player.NONE 
                        and all(element == slice[0] for element in slice)), None)
        return winner if winner is not None and self.__empty_spots > 0 else None

        # for slice in slices:
        #     if slice[0] is not Player.NONE and all(element == slice[0] for element in slice):
        #         return slice[0]
        #     return Player.NONE
        # else:
        #     return None
    
    @property
    def n(self):
        return self.__n
    
    """
    Returns the list of all available spots
    """
    def available_spots(self) -> List[Spot]:
        # return [(i, j) for i in range(self.__n) for j in range(self.__n) 
        #         if self.__board[i][j] != Player.NONE]
        return [(1 + index // self.n, 1 + index % self.n) for index, element in enumerate(self.__board) if element == Player.NONE]

    def __str__(self):
        slices = (slice(s, s + self.n) for s in range(0, self.n ** 2, self.n))
        return "\n".join(" ".join(map(str, self.__board[slice])) for slice in slices)

class Settings:
    pass

class TicTacToe:
    """
    Tic Tac Toe class

    Methods
    -------
    play():
        Begins the game of Tic Tac Toe
    """
    def __init__ (self, n: int = 3):
        self.__board = Board(n)        
        self.__parser = InputParser(prompt='Where do you want to mark on the board? ')

    def play(self):
        """
        Plays the game
        """
        # Beginning 
        choice = { Player.X: Player.O, Player.O: Player.X }
        choice_func = { Player.X: self.__computer_choice, Player.O: self.__computer_choice }
        prev_player = Player.X
        game_over = False

        winner = None

        while winner is None:
            input_func = choice_func.get(prev_player)
            prev_player = choice.get(prev_player)
            spot = input_func()

            self.__board.place(prev_player, spot)
            winner = self.__board.winner()

            print(f"{self.__board}\n")
            time.sleep(.5)
            print(21*"=")
            #input("The board right now... ")

        print("winner: ", winner)
        # spot = self.__player_choice()
        # self.__board.place(Player, spot)

    def __player_choice(self) -> Spot:
        #call input parser
        def _get_user_input():
            while True:
                yield self.__parser.parse_input()
        return next(dropwhile(partial(is_, None), _get_user_input()))        
    
    def __computer_choice(self) -> Spot:
        return random.choice(self.__board.available_spots())
